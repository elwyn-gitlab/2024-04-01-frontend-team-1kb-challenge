# frontend team april fools 1kb challenge

An "april fools" activity for the frontend team to try build something fun within 1kb (gzipped!).

I made a placeable black hole which sucks in elements from the webpage.

### How to run it

Copy paste the built artifact into a browser console, and then click somewhere on the page to place the black hole.

[artifacts/main/raw/dist/main.min.js](https://gitlab.com/elwyn-gitlab/2024-04-01-frontend-team-1kb-challenge/-/jobs/artifacts/main/raw/dist/main.min.js?job=build)

[artifacts/main/raw/dist/main.min.js.gz](https://gitlab.com/elwyn-gitlab/2024-04-01-frontend-team-1kb-challenge/-/jobs/artifacts/main/raw/dist/main.min.js.gz?job=build)