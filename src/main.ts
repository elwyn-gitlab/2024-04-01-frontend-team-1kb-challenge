document.body.style.cursor = `url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'  width='40' height='48' viewport='0 0 100 100' style='fill:black;font-size:24px;'><text y='50%'>⚫</text></svg>") 16 0,auto`
addEventListener('click', createBlackHole);

function createBlackHole(e: MouseEvent) {
    const {clientX, clientY} = e;
    const x = clientX - 25;
    const y = clientY - 25;
    removeEventListener('click', createBlackHole);
    document.body.style.cursor = '';

    const blackholeEl = document.createElement('div');
    blackholeEl.classList.add('bh');
    blackholeEl.setAttribute('style', `top: ${y}px;left: ${x}px;position: fixed;border-radius: 50%;background-color: #000;width: 50px;height: 50px;`);
    document.body.appendChild(blackholeEl);

    const allElements = Array.from(document.querySelectorAll<HTMLElement>('*'))
        .filter(el => !el.classList.contains('bh'))
        .filter(el => ['H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'P', 'A', 'SPAN', 'BUTTON'].includes(el.tagName));

    allElements
        .forEach(el => {
            const {left, top, width, height} = el.getBoundingClientRect();
            const {marginTop, paddingTop, marginLeft, paddingLeft} = window.getComputedStyle(el);

            el.dataset.position = JSON.stringify({
                top: top - ((parseFloat(marginTop) + parseFloat(paddingTop))),
                left: left - ((parseFloat(marginLeft) + parseFloat(paddingLeft))),
                width,
                height
            });
        });

    allElements.forEach(el => {
        const {top, left, width, height} = JSON.parse(el.dataset.position!);
        el.setAttribute('style', `position: fixed; top: ${top}px; left: ${left}px; width: ${width}px; height: ${height}px;`);
    });

    allElements.forEach(el => generateOrbitAnimation(el, x, y));
}
function generateOrbitAnimation(el: HTMLElement, targetX: number, targetY: number): void {
    const { left, top, width, height } = el.getBoundingClientRect();
    const offsetX = targetX - (left + width / 2);
    const offsetY = targetY - (top + height / 2);
    const distance = Math.sqrt(offsetX ** 2 + offsetY ** 2);
    const maxDistance = Math.sqrt(window.innerWidth ** 2 + window.innerHeight ** 2);
    const duration = 1000 + (distance / maxDistance) * 2000;

    el.animate([
        {
            transform: 'translate(0px, 0px) rotate(0deg) scale(1)',
            opacity: 1,
            offset: 0,
        },
        {
            transform: `translate(${offsetX / 2}px, ${offsetY / 2}px) rotate(180deg) scale(0.8)`,
            opacity: 0.8,
            offset: 0.5,
        },
        {
            transform: `translate(${offsetX}px, ${offsetY}px) rotate(360deg) scale(0)`,
            opacity: 0,
            offset: 1,
        },
    ], {
        duration,
        iterations: 1,
        fill: 'forwards',
        easing: 'ease-in',
    });

    setTimeout(() => el.remove(), duration);
}